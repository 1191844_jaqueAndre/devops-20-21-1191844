# Class Assignment 5, Part 2 Report - CI/CD Pipelines with Jenkins

The goal of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle 
"basic" version (developed in CA2, Part2).

Below you can find the description of the steps taken to complete this task. I've created the following issues on
Bitbucket:

![1.PNG](Img/1.PNG)

## 1. Description of the implementation of the requirements

### 1.1. Creating a new job

To get this pipeline working, I first had to create a new job. On the left side menu, I clicked on "New job", 
entered a name for it (in this case, "CA2_Part2_Gradle") and selected the pipeline option.

![2.PNG](Img/2.PNG)

Here are the pipeline's configurations for this job:

![3.PNG](Img/3.PNG)

### 1.2. Adding plugins

Before running the pipeline, one very important step was install a few plugins, so that it could run all the 
stages needed for the assignment. On the left sided menu, I clicked on "Manage Jenkins", followed by "Manage plugins":

![4.PNG](Img/4.PNG)

![5.PNG](Img/5.PNG)

Next, I searched for "HTML Publisher plugin" and installed it:

![6.PNG](Img/6.PNG)

I also needed the "Docker Pipeline" plugin:

![7.PNG](Img/7.PNG)

All the needed plugins were installed, so I restarted jenkins to make them all usable.

### 1.3. Adding credentials for docker-hub

The last very important step to complete before running the pipeline is to set the credentials for the docker-hub 
repository, where I will publish the docker image to.

On the left side menu, I clicked on "Manage Jenkins", followed by "Manage Credentials". I selected the "Jenkins" Store:

![8.PNG](Img/8.PNG)

On the left side menu, I clicked on "Add Credentials" and filled in the fields with my docker ID as the username, and 
password. The ID will correspond to an internal ID that will be recognized by jenkins when mentioned on jobs and 
configurations:

![9.PNG](Img/9.PNG)

### 1.4. Editing the Jenkinsfile

After much trial and error (emphasis on the error), this is the final version of the Jenkinsfile I used in this 
assignment:

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'ja-bitbucket-credentials', url:
                'https://bitbucket.org/1191844_jaqueAndre/devops-20-21-1191844/'
            }
        }
        stage('Assemble') {
            steps{
                dir('CA2\\Part 2\\Demo_Gradle') {
                    echo 'Assembling...'
                    bat './gradlew assemble'
                }
            }
        }
        stage('Test') {
            steps {
                dir('CA2\\Part 2\\Demo_Gradle') {
                    echo 'Testing...'
                    bat './gradlew check'
                }
            }
        }
        stage('Javadoc'){
            steps{
                dir('CA2\\Part 2\\Demo_Gradle') {
                    echo 'Generating the javadoc...'
                    bat './gradlew javadoc'
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/docs/javadoc', reportFiles: 'index.html', reportName: 'Javadoc', reportTitles: ''])
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts artifacts: 'CA2\\Part 2\\Demo_Gradle\\build\\libs\\*'
            }
        }
        stage('Docker Image') {
            steps {
                dir('CA2\\Part 2\\Demo_Gradle') {
                    script{
                        echo 'Publishing Docker image...'
                        dockerImage = docker.build("jaquelineandre/devops20-21:${env.BUILD_ID}")
                        docker.withRegistry('https://registry.hub.docker.com/', 'dockerCredentials') {
                            dockerImage.push()
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            junit '**\\build\\test-results\\test\\*.xml'
        }
    }
}
```

- The **"checkout"** stage allows jenkins to clone my remote repository, using my Bitbucket credentials;
- The **"assemble"** stage in the pipeline runs the gradlew assemble command, so that the demo gradle project can be
  compiled and the archive files can be produced;
- The **"test"** stage executes the project's unit tests;
- The **"javadoc"** stage in the pipeline runs the gradlew javadoc command, that generates the javadoc for the project, and
  publishes it as an HTML report, adding it to the job and build pages;
- The **"archiving"** stage archives in Jenkins files generated during Assemble, in this case, it archives the project's
  war file.
- The **"docker image"** stage builds a docker image from the dockerfile that exists in the root of the project (in this
  case, on the same location as the Jenkinsfile), and publishes the new image to my docker-hub repository, using my
  credentials. I used the Dockerfile for the web VM from the CA4 project, by copying and pasting it into the root of 
  this project.
- Finally, the post script publishes in Jenkins the test results and lets it track them.

The most difficult stage to get right was definetely the "Docker-Image" one, especially the command to publish the 
image on Docker-Hub.

### 1.5. Running the pipeline

Now all I had to do was select the job, click on "Build now" and wait. After 34 failed builds, the 35th was finally 
successful:

![10.PNG](Img/10.PNG)

From the "Javadoc" stage, we have now a link on the left side menu that allows us to check the project's javadoc:

![11.PNG](Img/11.PNG)

When clicking on the link, an HTML report opens up:

![13.5.PNG](Img/13.5.PNG)

From the "Archiving" stage, we can see the generated artifacts:

![14.PNG](Img/14.PNG)

The war file can be viewed and also downloaded.

Finally, for the "Docker image" stage, we can go to the Docker-hub repository and check that the image was 
published there:

![15.PNG](Img/15.PNG)


## 2. Analysis of the alternative

TeamCity is a build management and continuous integration and deployment server from JetBrains. It supports a 
variety of different version control systems and build runners. 

Some of its features that make it one of the best Jenkins alternatives are:

- It is well documented and provides easy installation.
- It provides integration with many tools like Docker, Maven, etc.
- It offers well-defined APIs available for extension.

|           FEATURE         | TEAMCITY | JENKINS |
|---------------------------|----------|---------|
| Product Type              | On-Premise | Self-hosted/On-Premise |
| Setup and Installation    | Easy | Easy |
| Ease of use               | User-friendliness available with out-of-the-box usability | Comparatively less user-friendly as the focus is on functionality than appearance |
| Official Support          | Yes | No official support but extensive support on IRC, Jenkins Forum, and other support channels |
| Plugin ecosystem          | Not so rich plugin ecosystem | Thrives on plugins (close to 1500+ plugins currently available) |
| Reporting                 | Yes | Yes |
| Build Pipelines           | Yes, Allows defining pipelines using a Kotlin-based DSL (Domain Specific Language) | Yes, Support for custom pipelines through Jenkins Pipeline DSL |
| Integration               | Yes, Amazon EC2, VMWare vSphere, Google Cloud, etc.Key Integrations: Docker, Maven, Visual Studio Team Services, NuGet, VCS Hosting Services | Yes, Amazon EC2, VMWare vSphere, Google Cloud, Atlassian Cloud, Slack, etc. through Jenkins Plugins |
| Online Documentation      | Yes, Blogs, Support Forum, Youtube channel, and more | Yes, Blogs, Support Forum, IRC, events, and more |
| Open Source or Commercial | Free for 100 build configuration | Free (open-source) |


## 3. Implementation of the alternative

### 3.1. Installing Team City

To use Team City, I used this Jetbrains
[link](https://www.jetbrains.com/teamcity/?gclid=CjwKCAjw_JuGBhBkEiwA1xmbRaUw9uLsY94DWtw2GPvOY-VhWs90PAXgMtDhN3LYeCfxugo0P1oX3BoCWcIQAvD_BwE&gclsrc=aw.ds)
and chose the Free Trial version.

After verifying my email and filling in some details, it asks to choose a domain:

![16.PNG](Img/16.PNG)

Afterwards, it will ask to login through github, bitbucket, gitlab or register with an username and password:

![17.PNG](Img/17.PNG)

I logged in with my bitbucket account, and clicked on "Create new project":

![18.PNG](Img/18.PNG)

It asks me to choose the repository and configure my Bitbucket credentials:

![19.PNG](Img/19.PNG)

![20.PNG](Img/20.PNG)


The next step is to check branch specifications and input the build configuration name:

![21.PNG](Img/21.PNG)

Now we can finally configure our pipeline's build steps.
TeamCity actually suggests some, based on the files from the repository:

![22.PNG](Img/22.PNG)

Since I wanted personalized steps, I clicked on "Build Steps" and chose to create a new one:

![23.PNG](Img/23.PNG)

The first step should be the "Checkout" one, but since TeamCity already has access to my bitbucket repository (because 
I've configured my credentials at the beginning, it has checked out behind the scenes, into the agent that will 
produce the builds), there's no need for it. So the first step to configure 
will be the equivalent to the "Assemble" stage.

Here are the configurations I used for the Assemble step:

![24.PNG](Img/24.PNG)

![25.PNG](Img/25.PNG)

Next, I've configured the Test task:

![26.PNG](Img/26.PNG)

![27.PNG](Img/27.PNG)

Before we head on to the Javadoc step, we first need to configure the general settings for artifact publishing, by 
clicking on the "General Settings", on the edit build page:

![28.PNG](Img/28.PNG)

After building the pipeline, TeamCity automatically publishes the artifacts that are generated on the path specified 
above. You can see the project's jar file if you go to the "Artifacts" tab on the build information:

![29.PNG](Img/29.PNG)

After some research, I figured out that there's no way with TeamCity to publish HTML reports from files generated from 
javadoc, not in the way we can with Jenkins. To go around this issue, I had to create new step to execute the 
"gradlew javadoc" command; it automatically generates html files in the build/docs folder. I added that folder path to 
the artifact paths in the general settings menu that was shown above, so we could access them:

![30.PNG](Img/30.PNG)

![31.PNG](Img/31.PNG)

![32.PNG](Img/32.PNG)

After building the project again, we can now see, in the Artifacts tab, that the javadoc html files are there:

![33.png](Img/33.PNG)

To make the javadoc appear in a tab of its own, in an HTML report style, we have to go to "Project Settings | Report 
Tabs" and click on "Create new build report tab":

![34.png](Img/34.PNG)

Edit the report tab settings:

![35.png](Img/35.PNG)

After running the project again, a new tab showed up:

![36.png](Img/36.PNG)

And when clicking on it, we can see the HTML report:

![37.png](Img/37.PNG)

Next step: Docker build. As we did before, we create a new build step, now with the following configurations:

![38.png](Img/38.PNG)

Once the new build passes, a new tab appears for the Docker information:

![40.PNG](Img/40.PNG)

The last step now is to publish the Docker image into my Docker-hub repository. To do so, we first have to add a new 
connection to the docker hub registry, in the edit project configurations menu:

![39.PNG](Img/39.PNG)

Next, we have to add a build feature, of type "Docker Support", in the edit build configurations menu:

![42.PNG](Img/42.PNG)

This way, TeamCity will be able to login to the docker-hub repository before the build starts and log out of it after the build. 
Docker support also enables the Docker events' monitoring: such operations as docker pull and docker run will be 
detected by TeamCity.

Finally, all we have to do now is add a new step to publish the docker image:

![41.PNG](Img/41.PNG)

Here is the final build result:

![44.PNG](Img/44.PNG)

And the Docker image, published on my Docker-hub repository:

![45.PNG](Img/45.PNG)

## 4. Final considerations

This assignment was very useful to learn how to use CI/CD tools (although in a brief way), and in particular Jenkins, 
which is one of the most popular ones. It is very simple to use, and can be very adaptable to one's needs through 
the vast choice of plugins available.

TeamCity has a very nice and intuitive interface, with the advantage of predicting possible build steps for the 
pipeline. The configuration of new build steps is also very easy and quick. It allows a seamless integration with 
Docker, with different repositories and build tools. It has a whole world of possibilities that I haven't had the 
chance to explore, but I enjoyed working with it very much and hope to have the chance to explore it more in the future.












