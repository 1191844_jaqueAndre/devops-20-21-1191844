# Class Assignment 5, Part 1 Report - CI/CD Pipelines with Jenkins

**Jenkins** is a free and open source automation server. It helps automate the parts of software development related to 
building, testing, and deploying, facilitating continuous integration and continuous delivery (CI/CD).

It supports version control tools, including AccuRev, CVS, Subversion, Git, Mercurial, Perforce, ClearCase and RTC, 
and can execute Apache Ant, Apache Maven and sbt based projects as well as arbitrary shell scripts and Windows batch 
commands.

A wide set of plugins (over 1600) is available for integrating Jenkins with most version control systems and bug 
databases. Many build tools are supported via their respective plugins. Plugins can also change the way Jenkins 
looks or add new functionality.

One of its plugins is the Pipeline one, that is enabled by default. It allows us to write build instructions using a 
domain specific language based on Apache Groovy.

The goal of this assignment is to create a simple pipeline using Jenkins and the "gradle basic demo" project, 
present in the repository, in the CA2 part 1 assignment folder.

Below you can find the description of the steps taken to complete this task. I've created the following issues on
Bitbucket:

![1.PNG](Img/1.PNG)

## 1. Testing the pipeline

I decided to create an experimental job first, so I could test the pipeline and make sure the script to use in the 
Jenkinsfile had the correct syntax and that there were no issues with it, before sending it to my repository.

### 1.1. Creating a new job on Jenkins

To access jenkins, I opened a command line, changed the current working directory to the folder where my jenkins.jar 
file was located and ran the command:

`java -jar jenkins.war`

Now that the server had started, I opened the browser on localhost:8080:

![0.PNG](Img/0.PNG)

Before doing anything else, I needed to configure the credentials for my repository on Jenkins, since it is private, 
so it can access it. To do so, I clicked on "Manage Jenkins", "Manage Credentials" under "Security", clicked on the 
Jenkins store and on "add some credentials".

![0.2.PNG](Img/0.2.PNG)

The Username and Password are the ones used to login on Bitbucket, and the ID will correspond to an internal ID that 
will be recognized by jenkins when mentioned on jobs and configurations.

Next, to create a new job, I clicked on "New Item" on the left menu, chose the name "Test", and the "Pipeline" option.

![0.1.PNG](Img/0.1.PNG)

### 1.2. Editing the script template

The goal for this step is to define the following stages in the pipeline:

1. **Checkout:** Checkout the code from the repository;
2. **Assemble:** Compile and produce the archive files with the application;
3. **Test:** Executes the unit tests and publish in Jenkins the test results;
4. **Archive:** Archives in Jenkins the archive files (generated during Assemble).

I've used a template script that was on the teacher's slides as a starting point, so I could later on use it to create 
the Jenkinsfile I needed:

![2.PNG](Img/2.PNG)

I had to make a few changes to this template, in order to have all the needed stages:

- Updated the credentialsId and url to the credentials to my repository;

![3.PNG](Img/3.PNG)

- Replaced the 'Build' step by the 'Assemble' step, replaced the `sh` command by the `bat` command (since my OS is 
  Windows), and replaced the gradle's build task by a task that only builds the project and doesn't execute the tests:

![4.PNG](Img/4.PNG)

- Added a 'Test' step, to execute the project's unit tests. I could have used the command `test` instead of `check`, 
  since they both run all unit tests. The difference is that `check` depends on `test`and aggregates other 
  verification tasks.

![5.PNG](Img/5.PNG)

- Changed the path for the 'Archiving' stage:

![6.PNG](Img/6.PNG)

- Finally, I added a script to publish the test results in Jenkins:

![7.PNG](Img/7.PNG)

This will always grab the test results and let Jenkins track them, calculate trends and report on them. A Pipeline 
that has failing tests will be marked as "UNSTABLE", denoted by yellow in the web UI.


### 1.3. Building the pipeline

The script was hopefully ready, I clicked on "Save" and then on "Build now" on the left side menu. Unfortunately, 
the first builds were not successful, as the script syntax was not easy for a first timer. I had some errors mainly 
over how to correctly describe folder paths, how to change directories and how to use the JUnit step.

After many failed builds, clouds and thunder, I finally reached a point where the script was right, and the 
build finally passed:

![8.PNG](Img/8.PNG)

On the bottom right area of the screen above, you can see the build history. The first one failed and the second one 
was successful. You can also see a list of archived artifacts, that include the projects zip and tar files. There's 
also a stage view, where we can check all of the pipeline's stages, the time they took to build and if they were 
successful or not. Lastly, we can see the test result trend. There's a blue dot that indicates that all the 
tests passed.


# 4. Configuring the pipeline from the repository source code

It was time to add a new job, so I could create a pipeline that would be executed from the remote repository this 
time. First, I needed to have a Jenkinsfile on the root of my repository. I copied the script used on the test pipeline 
and pasted it onto a new file. Next, I added it to my project's repository.

Then, I took the same steps to create a new job as I did before: on the dashboard, I clicked on "New Item":

![9.PNG](Img/9.PNG)

I named this job "Gradle Basic Demo" and chose the "pipeline" option. On the pipeline configuration, I chose "Pipeline 
script from SCM" along with the following options:

![10.PNG](Img/10.PNG)


All that was left to do was click "Build Now". Success!

![11.PNG](Img/11.PNG)


When hovering over a particular stage on the stage view, a button "Logs" will appear, that will show you the stage 
logs. This is very useful to check important events that happen in that stage.

![12.PNG](Img/12.PNG)

If you click on a particular build number, and then on "Console Output", you'll be able to see the complete text log 
of output from the execution, including any clues surrounding failure status, which was particularly handy in my case.

![13.PNG](Img/13.PNG)
