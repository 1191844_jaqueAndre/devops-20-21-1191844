# Class Assignment 3, Part 2 Report -   Virtualization with Vagrant

The main goals for this assignment are to use Vagrant to set up a virtual environment to execute an application, 
using 2 virtual machines; and doing the same, but using an alternative hypervisor to VirtualBox.

Vagrant is a tool for building and managing virtual machine environments in a single workflow. Vagrant is written in 
the Ruby language, but its ecosystem supports development in a few other languages. It may work on different 
hypervisors, such as VirtualBox, KVM, Hyper-V, VMware and AWS.


## 1. Description of the implementation of the Requirements

Before starting with the actual implementation, I've added issues in Bitbucket, to organize the tasks ahead:

![Issues.PNG](Images/Issues.PNG)

### 1.1. Installing Vagrant

- To install Vagrant, you can click [here](https://www.vagrantup.com/downloads) and download the appropriate version 
  for your OS. Open the downloaded file and follow the instructions. To check that it was installed successfully, you 
  can check the vagrant version in a terminal/console, by typing the command `vagrant -v`.
  
  
- To initialize a new project, create a folder where you want the project to be. Now, we need a configuration file 
  for it. In this case, we'll use a Vagrantfile provided by the teacher's 
  [repository](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/). I've just downloaded the file 
  and pasted it inside the project's folder. If you happen to want a new Vagrantfile, just type the following 
  command in your terminal: `vagrant init envimation/ubuntu-xenial`. This will create a VM with Ubuntu 10.04 already 
  installed. You can replace "envimation/ubuntu-xenial" with another ubuntu box if you wish.
  

- Vagrant is meant to run with one Vagrantfile per project, and its primary function is to describe the type of 
  machine required for a project. This Vagrantfile firstly specifies the ubuntu verson it is using (ubuntu-xenial), 
  then it has a provision section, with some software that will be common to both VMs. The next two sections have
  configurations for each VM:

  - **db**: specifies the used box (again, ubuntu-xenial), IP address, port mapping, downloads and installs H2 once, 
    runs a
    command to initiate H2.
  - **web**: specifies the used box, IP address, ram memory (1024 MB), port mapping, installs needed software (git, 
    nodejs, 
    npm, tomcat8), clones the repository of your choice, changes, the current working directory to the project's one, 
    gives execution rights to the gradlew command, builds the project and copies the generated war file to Tomcat's 
    webapps folder.
    

- Before we can go on, we need to make a quick change in the Vagrantfile:

  - Change the repository path we want to clone to the following one: https://bitbucket.org/1191844_jaqueAndre/devops-20-21-1191844.git
  - On the line below, on the change directory command, make sure to replace the path with the project's directory 
    one. Since Ruby does not work well if the path to your project contains spaces, I'll break the change directory 
    command into three parts (If your path does not contain spaces, you could just use `cd CA2/Part_2/Demo_Gradle`):
    
    ```
    cd devops-20-21-1191844/CA2/
    cd "Part 2"
    cd Demo_Gradle
    ```

### 1.2. Making the spring application use the H2 server in the db VM

- To add support to build the .war file, we need to add the following line `id 'war'` to the plugins section on the 
  build.gradle file. Next, add the command `providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'` 
  on the dependencies section. We'll then need to create a new java class on the 
  src/main/java/com/greglturnquist/payroll path, called "ServletInitializer.java":
  
  ```
  package com.greglturnquist.payroll;
  
  import org.springframework.boot.builder.SpringApplicationBuilder;
  import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
  
  public class ServletInitializer extends SpringBootServletInitializer {
  
      @Override
      protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
          return application.sources(ReactAndSpringDataRestApplication.class);
      }
  }
  ```

- To add support to the H2 console, add the following lines to the application.properties file 
  (src/main/resources/application.properties):
  
  ```
  spring.datasource.url=jdbc:h2:mem:jpadb
  spring.datasource.driverClassName=org.h2.Driver
  spring.datasource.username=sa
  spring.datasource.password=
  spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
  spring.h2.console.enabled=true
  spring.h2.console.path=/h2-console
  spring.h2.console.settings.web-allow-others=true
  ```
  
- Now, we need to alter the application's context path: on the app.js file (src/main/js/app.js), we need to change 
  the following method:
  
  ```
  	componentDidMount() { // <2>
		client({method: 'GET', path: '/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
  ```
  ...so that it looks like this (changed the path for the GET method):

    ```
  	componentDidMount() { // <2>
		client({method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
  ```

- On the index.html file (src/main/resources/templates/index.html), remove the forward slash on the reference to the 
  main.css file, so that it looks like this:
  
  ```
  <link rel="stylesheet" href="main.css" />
  ```
  
- Let's add some settings for the remote H2 database: on the application.properties file, comment out the following 
  line:
  
  ```
  #spring.datasource.url=jdbc:h2:mem:jpadb
  ```
  ...and add these two :

  ```
  spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
  spring.jpa.hibernate.ddl-auto=update
  ```

- Finally, let's commit all these changes to our repository.

### 1.3. Starting the VM and running the application

- To start the VM, type the following command from your project's folder: `vagrant up`. It might take a while, since 
  new software will be downloaded.
  
  
- The application is now ready! To open it, you can access either of these links:
  
    - http://localhost:8080/demo-0.0.1-SNAPSHOT/
    
    - http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
  
  You should get something like this:

  ![Webapp.PNG](Images/Webapp.PNG)


- You can also open the H2 console using one of the following urls (you may use "jdbc:h2:tcp://192.168.33.11:9092/.
  /jpadb" as the connection string):

    - http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
  
    - http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
  
  You should see something like this:


  ![Console.PNG](Images/Console.PNG)

  After clicking on "Connect" and clicking on "EMPLOYEES", followed by "run", you can see the table with the employees' 
  information:


  ![ConsoleData.PNG](Images/ConsoleData.PNG)

## 2. Analysis of an alternative hypervisor - Hyper-V

Microsoft Hyper-V, briefly known before its release as Windows Server Virtualization, is a native hypervisor; it can 
create virtual machines on x86-64 systems running Windows. It allows users to create virtual computer environments, 
run and manage multiple operating systems on a single physical server.


### 2.1. Hyper-V’s suite of management tools

Hyper-V’s suite of management tools makes it possible to:

- Create and delete virtual machines.

- Monitor uptime, bandwidth utilization, CPU and RAM consumption.

- Perform backups.

- Allocate resources.

Hyper-V can run on Windows or on Windows Server, and it has different functions available on each:

- Windows Server:
  - Live migration of virtual machines from one host to another
  - Hyper-V Replica
  - Virtual Fiber Channel
  - SR-IOV networking
  - Shared .VHDX
  
- Windows 10:
  - Quick Create and the VM Gallery 
  - Default network (NAT switch)


### 2.2. Benefits of Hyper-V

- It’s cost-effective: the basic Hyper-V tools are free with an enterprise agreement. You will have to pay for more 
  advanced Hyper-V functions and licensing, but even so, Hyper-V still has an edge over its competitors when it 
  comes to price.
  

- The capability to create a virtual switch comes at no extra cost. A virtual switch allows all your VMs to 
  communicate with each other, making it an integral part of virtual networking. Virtual switches are "intelligent", 
  meaning they inspect data packets before routing communication. The capability to configure traffic in this way 
  improves security within your virtual environment.
  

- Hyper-V supports multiple operating systems. Since Microsoft supports multiple operating Hyper-V systems, 
  including Linux, you are not limited exclusively to its native OS. But, be sure to reference best practices and 
  support documentation for proper integration of other systems to avoid any issues.
  
  
- It simplifies live migrations. This Hyper-V feature allows you to move running VMs from one Hyper-V host to 
  another without downtime. Live migrations are simple and promote continuous operation of your network.
  

- It integrates seamlessly with other Microsoft products.

### 2.3. VirtualBox vs Hyper-V

- **Hypervisor type:** Hyper-V is a type 1 hypervisor (also called a bare metal hypervisor), and runs directly 
  on a computer’s hardware. When a physical computer (a host) starts, a Hyper-V hypervisor takes control from BIOS 
  or UEFI. Then, Hyper-V starts the management operating system, which can be Hyper-V Server, Windows, or Windows 
  Server. Virtual machines can be started manually by user or automatically, depending on its settings. VirtualBox 
  is a type 2 hypervisor that is sometimes called a hosted hypervisor. It runs on the operating system (OS) and is 
  already installed on a host. When a physical computer starts, the operating system installed on the host loads 
  and takes control. A user starts the hypervisor application (VirtualBox in this case) and then starts the needed 
  virtual machines. VM hosted processes are created.
  

- **Platforms:** Hyper-V can only run on Windows family operating systems. Hyper-V feature is built-in from Windows 
  8 onwards, and is also available as a server role from Windows Server 2008 to later versions, although it does not 
  come as an option on Windows 10 Home. Virtual Box can run on a higher number of operating systems such as Linux, 
  Windows, Solaris, and macOS.
  

- **Support of guest operating systems:** Guest operating system is the OS that is installed and runs on a VM. A 
  guest OS and host OS may differ. Hyper-V can host VMs with Windows, Linux, and FreeBSD guest operating systems. 
  VirtualBox supports more guest operating systems than Hyper-V: Windows, Linux, FreeBSD, Solaris, Mac OS, and others.
  

- **Performance:** VirtualBox is one of the slowest virtual machine options available, which may not be very 
  noticeable if you have decent hardware. On the other hand, Hyper-V users may notice some performance issues on 
  other areas, because it runs at the BIOS level rather than as software within the OS, so the virtualization is 
  always "on," even if you are not using a virtual machine.
  
  
- **Snapshots and Checkpoints:** Although the platforms use different names, Snapshots and Checkpoints are very 
  similar tools. They allow you to take an image of the virtual machine in its current status. The image preserves 
  the virtual machine, allowing you to return to that specific moment.
  
  
- **Seamless mode:** VirtualBox uses seamless mode to integrate the virtual machine environment into the host 
  operating system. Seamless mode strips away the additional virtual machine window and menus, making it feel as if 
  the guest operating system is part of the host. Unfortunately, Hyper-V does not support this.
  

## 3. Implementation of the alternative

For this part of the assignment, we'll be using Hyper-V, running on Windows 10 Pro.

### 3.1. Installing Hyper-V

- The requirements to install the Hyper-V role are:
  - Windows 10 Enterprise, Pro, or Education.
  - 64-bit Processor with Second Level Address Translation (SLAT).
  - CPU support for VM Monitor Mode Extension (VT-c on Intel CPUs).
  - Minimum of 4 GB memory.

The Hyper-V role cannot be officially installed on Windows 10 Home. There are some ways to work around this, but
there's no guarantee that it will work through windows updates.


- Hyper-V can be enabled in many ways including using the Windows 10 control panel, PowerShell or using the 
Deployment Imaging Servicing and Management tool (DISM). You can consult Microsoft Documentation 
[here](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v) if you wish to 
know more about each way. I've enabled it throught the "Apps and Features" settings:
  - Right click on the Windows button and select ‘Apps and Features’.
  - Select Programs and Features on the right under related settings.
  - Select Turn Windows Features on or off. 
  - Select Hyper-V and click OK.
  
  ![ActivateHyperV](Images/ActivateHyperV.PNG)

  Afterwards, you'll just have to restart your device and it's done!

### 3.2. Vagrantfile changes

- As we did for VirtualBox, you'll need to create a new folder where you want your project to be located. I'll be 
  using the same Vagrantfile for this as the one I've used for VirtualBox, but a simple change needs to be made 
  to adapt it to Hyper-V:
  
  - Choose a box that is compatible. I've chosen the "bento/ubuntu-16.04" box. You'll have to alter the configuration 
    on three places in the vagrantfile:
  
  ```
  Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-16.04"
  ```

  ```
  config.vm.define "db" do |db|
  db.vm.box = "bento/ubuntu-16.04"
  ```

  ```
  config.vm.define "web" do |web|
  web.vm.box = "bento/ubuntu-16.04"

  ```
  
### 3.3. Set environment variable

- The first setback I've encountered was a compatibility error (incompatible character encodings: CP850 and 
Windows-1252). After some research, I understood why: when running the command "vagrant up", vagrant will want to 
visit the ".vagrant.d" folder, in your user directory. But since the user profile is named in Windows 1251 encoding, 
the Ruby script won't recognize it properly.
  

- Fortunately, the solution for this is quite simple: setting the environment variable `VAGRANT_HOME`. I used the 
  command line as an administrator to do so, by running `set VAGRANT_HOME=C:\HashiCorp\Vagrant` and it solved the 
  issue. This will only last for the current session, though, so you'll have to repeat this step next time, unless 
  you set it as an environment variable permanently.
  

### 3.4. Starting the VMs and setting the IP addresses

- To start the VMs, open the command line as administrator, make sure you're in your project folder and run the command
  `vagrant up --provider hyperv`. We need to specify the provider here, as VirtualBox is the default provider in
  my machine's settings.

  Vagrant will eventually ask for the switch you'd like to attach to the hyperV instance (Default Switch or WSL - 
  choose Default Switch) and for your system's credentials to continue the booting of each VM.
  

- The IP address will be different from the one we configured for VirtualBox. With HyperV, you cannot configure a 
  static IP, so you'll have to check the IP for each VM each time you start it. A simple way to check them is by 
  going the HyperV manager and clicking on the VM and then, on Networking:
  
  
  ![CheckIPsManager.png](Images/CheckIPsManager.png)
  

  The IP address for the web VM in this session is 172.29.148.143 and for the db VM is 172.29.154.80.
  

- Note: Since HyperV cannot establish a static IP for each machine, it will ignore the IP configurations in the 
  Vagrantfile.
  

- Finally, we need to change the IP used in the application.properties file to the db IP. Inside the web VM (run 
  `vagrant ssh web`), change your current working directory to "devops-20-21-1191844/CA2/Part 
  2/Demo_Gradle/src/main/resources", where the application.properties is stored. To alter this file, run the command 
  `nano application.properties` and make the following change: 

  `spring.datasource.url=jdbc:h2:tcp://172.29.154.80:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE`


- Since you changed this configuration, you'll have to build the project again, by running `./gradlew clean build` 
  at the project's root.
  

- The you'll need to copy the war file to the webapps folder, by running `sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.
  war /var/lib/tomcat8/webapps`.


- Reload the vagrantfile so the new changes take effect: `vagrant reload`.

  
### 3.5. Time to access the web application and console

- Now that both VMs are up and running, let's access the web application by clicking on the following link:
  
    - http://172.29.148.143:8080/demo-0.0.1-SNAPSHOT/

  You should get something like this:


  ![Web_HyperV.png](Images/Web_HyperV.png)

- You can also open the H2 console using the following url (you may use "jdbc:h2:tcp://172.29.154.80:9092/.
  /jpadb" as the connection string):

    - http://172.29.148.143:8080/demo-0.0.1-SNAPSHOT/h2-console
  

  ![Console_HyperV.png](Images/Console_HyperV.png)


## 4. Final considerations

After using both VirtualBox and HyperV as hypervisors to accomplish the same tasks, I have to say that my preference 
is definitely with VirtualBox. It has the advantage of being a type 2 hypervisor, running on the host's OS, whereas 
HyperV might make applications in the host OS less performant, since it runs at the BIOS level and is always "on". 

VirtualBox is also easier to work with, in my opinion, and has a wider set of platforms it can run on, while 
HyperV is very restricted, by only running on Windows software. Since I only have the Windows 10 Home installed, I 
did not even have the option to use HyperV, so I had to borrow a laptop that had another version of Windows 10.

On the other hand, HyperV was not that much harder to handle, not many configurations had to be done to make both 
VMs run successfully. I'd say the hardest part was configuring the IP addresses, so I could access the console and 
the web application, and also figuring out the compatibility issues with character encoding.

All in all, this was a very useful assignment to gain insight about the main features of a hypervisor, how it can be 
used alongside vagrant, and managing two VMs at the same time.