# Class Assignment 3, Part 1 Report -   Virtualization with Vagrant

The goal of this assignment is to practice with VirtualBox and Ubuntu, using the same projects from previous 
assignments.

Virtual Machines (VM) may be useful in the following scenarios:

- Running multiple operating systems simultaneously
- Easier software installations
- Testing and disaster recovery
- Infrastructure consolidation.

VirtualBox is a general-purpose full virtualizer, developed by Oracle Corporation, for x86 hardware, targeted at
server, desktop and embedded use.

Presently, VirtualBox runs on Windows, Linux, Macintosh, and Solaris hosts and supports a large number of guest
operating systems including but not limited to Windows (NT 4.0, 2000, XP, Server 2003, Vista, Windows 7, Windows 8,
Windows 10), DOS/Windows 3.x, Linux (2.4, 2.6, 3.x and 4.x), Solaris and OpenSolaris, OS/2, and OpenBSD.

Below you can find the description of the steps taken to complete this task. I've created the following issues in 
Bitbucket:

![BitbucketIssues.PNG](Images/BitbucketIssues.PNG)

## 1. Creating the VM

To download VirtualBox, go to the following website: https://www.virtualbox.org/. The installation is very 
straightforward. Next, we'll create a new VM:

- Having the VirtualBox manager open, click on "New", write a name for the virtual machine, select the destination 
  folder, and the type of operating system you want to install on it (in this case, it will be Linux).
  
  
- Next, choose the amount of RAM you want allocated to the VM (2048MB will be enough). 

  
- Now, you'll have to decide what you want to do in regard to the hard disk: not add one, create a virtual hard disk 
  now, or use an existing one. For this assignment, we'll create a new virtual disk, of type VDI (VirtualBox Disk 
  Image). Choose the "Dynamically allocated" option for the storage on physical hard disk. For the size of the 
  virtual disk, you might choose anything from 1GB.
  
  
- We now have a virtual machine installed, but we still need to add an operating system. Go tho the following page: 
  https://help.ubuntu.com/community/Installation/MinimalCD and download the Ubuntu 18.04 version.
  
  
- In the VirtualBox's settings, click on storage, select the "Empty" option under "Controller: IDE", choose the mini.
  iso ubuntu file as your optical drive, tick the "Live CD/DVD" box, so that the file will be read when initiating 
  the machine, and click OK.
  
  
- Next, we'll want to set our network adaptors. Go back to settings, and click on "Network". We can leave adapter 1 as 
  it is by default and go to the adapter 2 option. We'll tick the "Enable Network Adapter" box and select the option 
  "Host-Only Adapter". To see your host network configurations, you can go to File -> Host Network Manager.
  We have now a connection that allows us to access the Internet through our host machine and a local network, to 
  which other VMs can connect.
  

- We're now ready to start our machine, by clicking on it and selecting start.

## 2. Installing Ubuntu

- The machine has read the Ubuntu CD-ROM when booting, so now it asks what you want to do with it. Select the 
  "Install" option.
  
  
- Next, you'll have to choose some set-up options, such as language for the installation process, location and keyboard 
  layout.
  
  
- For the network configuration, you may choose the "enp0f3" option as the primary network interface.


- Now, you may select a name for the computer, a country from which the system will get the Ubuntu file mirror 
  (I've selected Portugal), and the mirror file.
  
  
- Next, you'll see an option to input information about a proxy if you wish to add one. I've left it blank, as default.


- After taking some time to install the necessary software, the system will ask you information to set up an user 
  account. You'll have to input your name, a username, and a password, as well as confirm your timezone.
  

- Select the disk partition method "Guided - use the whole disk", and the disk you'll want to use. The system will 
  then ask you if you want to write these modifications on the disk, select "Yes".
  

- Finally, a message will appear, asking you to remove the CD_ROM that contains the installation program, so that 
  you can boot the new system instead of reinstalling it. To do so, you can go to the VirtualBox manager, click on 
  settings, select the storage menu, click on the mini.iso CD and remove it as the optical drive. 
  
  
- The system will now reboot, and the login options will appear. The system is now ready to use.


## 3. Setup Networking

- After logging in, you should update the packages repositories, using the command `sudo apt update`.


- To install the network tools, you may run the command `sudo apt install net-tools`.


- Now we should set up the IP that we'll use, by running the following command: `sudo nano /etc/netplan/01-netcfg.
  yaml` and write the following content:
  
    ```
      network:
         version: 2
         renderer: networkd
         ethernets:
            enp0s3:
               dhcp4: yes
            enp0s8:
               addresses:
                  - 192.168.56.5/24
    ```
  
To apply the new changes, you may run `sudo netplan apply`.


- Next, we should install openssh-server, so we can use ssh to open secure terminal sessions to the VM, from other 
  hosts. To do so, run the following command `sudo apt install openssh-server`. To enable password authentication 
  for ssh, you'll have to modify its configuration file: `sudo nano /etc/ssh/sshd_config`, by uncommenting the line 
  "PasswordAuthentication yes". You may then restart the ssh service: `sudo service ssh restart`.
  
  
- To help transfer files to and from the VM, we should install an ftp server: `sudo apt install vsftpd`, and then 
  enable write access for it, by modifying its configuration file: `sudo nano /etc/vsftpd.conf`, uncommenting 
  the line "write_enable=YES". Restart the vsftpd service by running the following command: `sudo service vsftpd 
  restart`.
  
  
- Now that the SSH server is enabled, we can now use it to connect to the VM. You can type `ssh joao@192.168.56.5`, 
  where "joao" should be replaced by the user name of your VM and the IP should be replaced by your VM's one.
  
  
## 3. Installing Git and Java


- To install Git, run the command: `sudo apt install git`.


- To install Java, run the command `sudo apt install openjdk-8-jdk-headless`.


## 4. Cloning, Building and Running the Spring Tutorial

- Create a new folder for the Spring Tutorial application, using the command `mkdir <directoryname>`. I've called 
  the folder "SpringTutorial".


- To clone the Spring Tutorial application, change the directory so you're inside the new folder and run the 
  following command:
`git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git`.
  
  
- To build and execute the application, change your current directory to the basic version : `cd 
  tut-react-and-spring-data-rest/basic` and then run the following command: `./mvnw spring-boot:run`. Since the 
  project has a maven wrapper, there is no need to install Maven in the VM, but if you still wish to do so, you can 
  run the command `sudo apt install maven`.
  
  
- You should now be able to open this application on a browser on your host computer, with the following url:  
  http://192.168.56.5:8080/
  
  
## 5. Cloning, Building and Running the Gradle_Basic_Demo Project


- To get this project's files, I've used FileZilla (you may download it here: https://filezilla-project.org/) 
  instead of simply cloning the project's repository, so I could practice with the program. 
  
  Once you have Filezilla open, you can input the server address, your username and password, as well as the port 
  number.
  Click on Quickconnect and the connection is ready. I've chosen the files I wanted from my machine and copied them 
  to the VM, into a new folder called "Gradle_Project". If you don't have the project in your local machine, you may 
  get them from this repository: https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/
  
  By changing the working directory to this new folder and running `ls -l`, I can see that all the files were transferred 
  successfully.

  
- Before we can build the project, we must modify permissions, so we can execute the needed files. To add execute 
  permission to all the files, we can use the command `chmod +x _R Gradle_Project`.
  
  
- Next, we can run the application, by typing the following command: `./gradlew build`. Again, we are using the 
  gradle wrapper, so there's no need to install gradle in the VM. Still if you want, you can install it by running 
  the command `sudo apt-get install gradle`.


- Now, to run the project's server, execute the following command: 
  
  `% java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001`

  You can replace the number 59001 by your server port number. If you see the message below, then the server is running 
  successfully:
  
  ![RunningServer.PNG](Images/RunningServer.PNG)

  
- On your host machine, make sure you have the project files (not only on the VM). Open the build.gradle file and on 
  the runClient task, replace 'localhost' with your VM's IP address. Save the file.
  

- Open a command line on your host machine, change the directory to where you have the project's files, run the 
  command `gradlew build`, followed by the command `gradlew runClient`. A chat box will appear. Once you input a 
  username in the chat box, you'll see that the server status on the VM will be updated with the following message: 
  
  ![NewUser.PNG](Images/NewUser.PNG)

  To open another chat box, you must open a new command line and repeat this process.