# Class Assignment 2, Part 1 Report -   Build Tools with Gradle

This assignment's goal is to use Gradle as the main build tool in a given project.

## 1. Description of the implementation of the Requirements

### Step 1 - Setting up the project

- Download the project you'll be working with in the following link:

  https://bitbucket.org/luisnogueira/gradle_basic_demo/


- Create a folder for this assignment in your local repository, called "CA2" and another folder inside this one, 
  called "Part 1". You may place the project files here and commit them to your remote repository (I'm using git and 
  a Bitbucket repository). If your VCS tool allows you to work in branches, you should create a new one for each of 
  these tasks.
  
  
- To experiment with the application, feel free to follow the instructions on the 
  [OriginalAppReadMe.md](OriginalAppReadMe.md) file.
  
  
- If you want, you can create issues in Bitbucket, to keep track of the tasks you have to accomplish. You should get 
  something like this:


  ![1.PNG](images/1.PNG)



### Step 2 - Adding a task to execute the server

- After creating your new branch, we are going to open the build.gradle file. This is where we are writing all of our 
  tasks. To execute the server for this application, the following task was added:


  ![2.PNG](images/2.PNG)


  Then you should load the gradle changes and run the task. You should see something like this happening in your 
  IDE's terminal:

  
  ![3.PNG](images/3.PNG)


  This means the task was executed successfully.


- Now it's time to commit the new configurations to your remote repository.

### Step 3 - Adding a unit test and updating the gradle script to execute it

- To accomplish this step, a new test class should be created under the path "src/test/java/basic_demo/AppTest.
  java". On this class, the following code was copied:
  
```
package basic_demo;

import org.junit.Assert;
import org.junit.Test;

public class AppTest {

    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        Assert.assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
```


- To make the test work, the junit 4.12 dependency had to be added on in the build.gradle file. To accomplish this 
  step, the last line you see on the picture below was added to the dependencies:
  
  
  ![4.PNG](images/4.PNG)


Now the test is running with no issues.
  
### Step 4 - Adding a new task type Copy

- This task's goal is to copy the contents of the src folder into a new backup folder. To do this, you may add the 
  following new code to your build.gradle file:
  

  ![5.PNG](images/5.PNG)


- After loading the new changes and running the task, you should get a new directory called "backup" that contains 
  the same folders and files as the src folder:
  

  ![6.PNG](images/6.PNG)

  
### Step 5 - Adding a new task type Zip

- This task's goal is to copy the contents of the src folder into a new zip file, that will serve as an archive of 
  the sources of the application. To do this, you can add the following code to your build.gradle file:
  

  ![7.PNG](images/7.PNG)


- You should then load the new gradle changes and run the task. You can now see a new folder containing a zip file 
  in your root directory: 
  
  
  ![8.PNG](images/8.PNG)


If you unzip this file, you get a copy of the files contained in the src folder.


## Final considerations

This assignment was very useful to learn the Gradle build tool and how it works, to understand the structure of a build.
gradle file and the purpose of tasks. It also made me practise writing in the build description language (DSL) 
needed to write such tasks and add the different configurations needed. The used DSL, based on Groovy, was in fact very 
intuitive to use and was not difficult to understand. 