/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobRole;
	private String emailAddress;

	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobRole, String emailAddress) {
		this.firstName = validateName(firstName);
		this.lastName = validateName(lastName);
		this.description = validateDescription(description);
		this.jobRole = validateJobRole(jobRole);
		this.emailAddress = validateEmail(emailAddress);
	}

	public String validateName(String name){
		if(name == null || name.isEmpty() || name.trim().length() == 0){
			throw new IllegalArgumentException("The name is not valid.");
		}
		return name;
	}

	public String validateDescription(String description){
		if(description == null || description.isEmpty() || description.trim().length() == 0){
			throw new IllegalArgumentException("The description is not valid.");
		}
		return description;
	}

	public String validateJobRole(String jobRole){
		if(jobRole == null || jobRole.isEmpty() || jobRole.trim().isEmpty()){
			throw new IllegalArgumentException("The job role is not valid.");
		}
		return jobRole;
	}

	public String validateEmail(String email){
		if(email == null || email.isEmpty() || email.trim().length() == 0 || !validateFormat(email)){
			throw new IllegalArgumentException("The email address is not valid.");
		}
		return email;
	}

	private boolean validateFormat(String input){
		boolean result = true;
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
				"[a-zA-Z0-9_+&*-]+)*@" +
				"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
				"A-Z]{2,7}$";
		Pattern pattern = Pattern.compile(emailRegex);
		if (!pattern.matcher(input).matches()) {
			result = false;
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
			Objects.equals(jobRole, employee.jobRole) &&
			Objects.equals(emailAddress, employee.emailAddress);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description, jobRole, emailAddress);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setJobRole(String jobRole){this.jobRole = jobRole;}

	public String getJobRole(){return jobRole;}

	public void setEmailAddress(String emailAddress){this.emailAddress = emailAddress;}

	public String getEmailAddress(){return emailAddress;}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
			", jobRole='" + jobRole + '\'' +
			", emailAddress='" + emailAddress + '\'' +
			'}';
	}
}
// end::code[]
