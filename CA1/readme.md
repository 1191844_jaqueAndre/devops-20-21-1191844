# Class Assignment 1 Report -  Version Control with Git


## 1. Analysis, Design and Implementation

The proposed assignment's goal is to manage changes made to an application's source code with Git and an alternative 
version control system. The source code was provided by a project from Github, and one of its functionalities is to 
allow a table to render in a browser. This table has information about employees, including the following fields : 
first name, last name and description. In a previous exercise, I added the field called "Job role" as well, with the 
intent to experiment and understand the purposes of the backend classes on this source code.

A tutorial for the react-and-spring-data-rest application can be found on the file [SourceCode_README.md](CA1/SourceCode_README.md).

The repository where this assignment was developed can be found on the following link:
https://bitbucket.org/1191844_jaqueAndre/devops-20-21-1191844/src/master/CA1/


#### 1.1 Version Control System

Version control systems (VCS) are software tools that help to track and manage changes to source code over time. 
There are many different VCS around, but they all have the same primary benefits:

1. A complete long-term change history of every file. This means every change made by many individuals over the 
   years. Changes include the creation and deletion of files as well as edits to their contents.
   
2. Branching and merging. This keeps multiple streams of work independent from each other while also providing the 
   facility to merge that work back together, enabling developers to verify that the changes on each branch do not 
   conflict.
   
3. Traceability. Being able to trace each change made to the software and being able to annotate each one with a 
   message describing the purpose and intent of the change can help with root cause analysis and other forensics. 

#### 1.2 Types of VCS

There are two main types of version control: centralized and distributed.

**Centralized version control -** With centralized version control systems, you have a single “central” copy of 
your project on a server and commit your changes to this central copy. You pull the files that you need, but you 
never have a full copy of your project locally. Some of the most common version control systems are centralized, 
including Subversion (SVN) and Perforce. 

**Distributed version control -** With distributed version control systems, you don't rely on a central 
server to store all the versions of a project’s files. Instead, you clone a copy of a repository locally so that 
you have the full history of the project. Two common distributed version control systems are Git and Mercurial.

#### 1.3 Git

Git is the most widely used modern free and open source distributed version control system in the world today . It 
works well on a wide range of operating systems and IDEs.

The main difference between Git and other VCS is that it thinks of its data more like a series of snapshots of a 
miniature filesystem. Every time you commit, or save the state of your project, Git basically takes a 
picture of what all your files look like at that moment and stores a reference to that snapshot. 

To be efficient, if files have not changed, Git doesn’t store the file again, just a link to the previous identical 
file it has already stored. Another benefit to git is that every developer's working copy of the code is also a 
repository that can contain the full history of all changes. It means you don’t need a network connection to create 
commits or inspect previous versions of a file, which makes Git fast. Its branching capabilities are also very 
popular, as the branches are cheap and easy to merge.

Due to all the benefits mentioned above, Git will be the primary VCS used for the assigment.

### Tutorial

As mentioned above, the Tutorial React.js and Spring Data REST application will be used in this assignment. 
- The main goal is to add an email field to the application, write unit tests for the creation of employees and 
  include validations of their attributes. This should be done in a specific branch, so as not to damage the stable 
  code that is in master. After this new feature is finished and tested, we will merge this branch into master and 
  tag the new version as v1.3.0. 

- Afterwards, we will create a second branch to add further validation to the email address, making it only accept 
  emails with the "@" symbol and a dot after the domain name. Since this is a minor change, after merging this second 
  branch into master, we'll tag the latest version with v1.3.1.

- Finally, we will mark the repository with the tag ca1, at the end of the assignment.

- Issues in Bitbucket should be created for the main tasks, but unfortunately, the version of Bitbucket used does not
  allow integration with Jira for the creation of issues.

#### Step zero - Environment setup

Before we can go on with the tutorial, you must have an IDE that is compatible with Git (like IntelliJ, for example) 
and have Git installed on your device (the latest version of git can be downloaded from https://git-scm.com/). Any 
additional set-up can be found here: https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup .

It is also essential that you have a repository set up. In this assignment, I'll be using my Bitbucket repository. 
The link for it can be found above.

The command line will be used for the git commands.

#### Step one - Clone a repository, set the remote repository and tag the project's initial version

- In the command line, we need to change the directory to the project's directory and then run the following command:

    `git clone https://github.com/spring-guides/tut-react-and-spring-data-rest/tree/master/basic CA1`

    This command will clone the repository found in the link (it's the repository that contains the project files we 
  need) into a directory called CA1, initializing a .git repository inside it, pulling down all the data from that 
  repository and checking out a working copy of the latest version. 


- Since we only needed the repository above to obtain the source code and it is not the remote repository we want to 
use to keep track of our changes, we'll have to set our remote repository by running the following command:

    `git remote set-url origin https://bitbucket.org/1191844_jaqueAndre/devops-20-21-1191844/src/master/CA1/`
  (you should use the link to your repository)


- To confirm you have now a clean working directory, with no untracked or modified files, you can run the command:

    `git status`

    You should see a message similar to this:
  
    ```
    On branch master
    Your branch is up to date with 'origin/master'.
    nothing to commit, working tree clean
    ```

- Finally, we should send all these files that are stored locally, to our remote bitbucket repository:

    `git push origin master`


- Now that we have all the files we need in our repository, we should tag the previous commit, since it is the first
version of our application. We'll use an annotated tag (v1.2.0):

    `git tag -a v1.2.0 -m "Initial version"`

#### Step two - Creating a branch for the email feature

- It is advised to create branches when developing new features, so as not to damage the stable code that is in master.
To do so in the command line, you'll have to type:

    `git branch email-field` (where "email-field" is the name of the branch)


- To start working on the new branch, run the command:

    `git checkout email-field`

#### Step three - Add support for email field, validations and testing

- To successfully run the application, you should first run the command:
  
    `./mvnw spring-boot:run`


- With the project open in your IDE, make sure you're in the right branch, find the Java class Employee, and add a new 
  private attribute with the name "emailAddress", of the String data type.


- In the same class, add the email property to the equals() and hashcode() methods.


- Add a getter and a setter methods for the email address.


- Add the email property to the toString() method.


- Add the validation methods for all the employee's properties.


- In the DatabaseLoader class, update the creation of the employees with the new email parameter.


- In the app.js file, update the table to include the email field.

To check the changes made above, you can consult the class 
[Employee.java](src/main/java/com/greglturnquist/payroll/Employee.java) and the file [app.js](src/main/js/app.js).

- Now we're ready to compile all the files (you can click on the maven tool, select Lifecyle and double-click the 
  compile option). To check if the changes are working well, you can go to the link http://localhost:8080/ .
  
    You should have something like this:
  
    ![browser img.PNG](images/browser img.PNG)

- Lastly, we should add some tests to check if the employees are being created smoothly and if the validation of 
  their attributes is working correctly. You can consult them here: [EmployeeTest.java](src/test/java/EmployeeTest.java) .
 

#### Step four - Commit, push all changes and tag the new version

- To see all the files that were modified just now, you can type again:

    `git status`


- To add each file, you'll have to run:

    `git add <filename>`


- To commit your changes with a message, you'll have to type:

    `git commit -m "Added email-field, validations and tests"`


- To push your changes to the remote repository email-field branch, you can run the command:

    `git push origin email-field`


- Once you're sure that you have stable code and are ready to merge your new feature into master, you can run the 
following commands:

    `git checkout master`
    `git merge email-field`
  

- Lastly, you can tag the last commit to mark the new version and send it to master:

    `git tag v1.3.0 -m "Added email field and validations`
    `git push origin v1.3.0`

#### Step five - Fixing bugs (invalid emails)

There's a bug in our code - we should not be allowing the creation of employees with emails that don't have the "@" 
sign or without the "." after the email domain. 

- To fix this, we'll create another branch from our master one and work from it:

    `git branch fix-invalid-email`
    `git checkout fix-invalid-email`
  

- Add the regex validation to the email validation method (see [Employee.java](src/main/java/com/greglturnquist/payroll/Employee.java)).


- Add more tests to verify the validation is done correctly (see [EmployeeTest.java](src/test/java/EmployeeTest.java)).


- You can now stage the modified files, commit and push them to the remote repository branch:
  
    `git add <filename1>`
    `git add <filename2>`
    `git commit -m "Fixed email bug"`
    `git push origin fix-invalid-email`


- Once you're sure the bug is fixed and the code is stable, you may merge the branch you were working on into master:

    `git checkout master`
    `git merge fix-invalid-email`
    `git push origin master`
  
  
- Last but not least, we can now tag the latest version with "v1.3.1", to signify a minor change:

    `git tag -a v1.3.1 -m "Fixed invalid email bug"`
    `git push origin v1.3.1`


## 2. Analysis of an Alternative

### 2.1 Apache Subversion

A possible alternative to Git might be Apache Subversion. Unlike Git, Subversion (SVN) is a centralized version 
control tool, and the most popular one. 

Being centralized, all files and historical data are stored on a central server. Developers can commit their changes 
directly to that central server repository. A "checkout" from this central repository will place a "working copy" 
on the user's machine (this is a snapshot from a certain version of the project on his disk). This means that only 
the files a developer is working on are kept on the local machine, and the developer must be online, working with 
the server. Users check out files and commit changes back to the server.

With SVN, work is comprised of three parts:

**- Trunk:** The trunk is the hub of your current, stable code and product. It only includes tested, unbroken code. 
This acts as a base where all changes are made from.

**- Branches:** Here is where you house new code and features. Using a copy of the trunk code, team members conduct 
research and development in the branch. Doing so allows each team member to work on the enhanced features without 
disrupting each other’s progress.

**- Tags:** Consider tags a duplicate of a branch at a given point in time. Tags aren’t used during development, but 
rather during deployment after the branch’s code is finished. Marking your code with tags makes it easy to review 
and, if necessary, revert your code.

### Git vs SVN

Here's how Git and SVN behave relating to different features:

- **Commit speed:** With Git, it’s faster to commit. Because you commit to the central repository more often in SVN, 
  network traffic slows everyone down. Whereas with Git, you’re working mostly on your local repository and only 
  committing to the central repository every so often.
  
- **Centralized vs distributed:** With Git, there's no single point of failure. With SVN, if the central repository 
  goes down or some code breaks the build, no other developers can commit their code until the repository is fixed. 
  With Git, each developer has their own repository, so it doesn’t matter if the central repository is broken. 
  Developers can continue to commit code locally until the central repository has been fixed, and then they can 
  push their changes.
  
- **Offline availability:** Unlike SVN, Git can work offline, allowing the team to continue working without losing 
  features if they lose connection.
  
- **Branching and merging:** Branches can be created, deleted and changed at any time, without affecting commits. They 
  are lightweight, since they are only references to a certain commit, but powerful. SVN branches are created as 
  directories inside a repository. Merges to the trunk can be problematic if other people are merging their 
  branches as well, causing sometimes conflicts, missing files and jumbled changes.
  
- **Access controls:** By default, git assumes all developers have the same permissions, while SVN allows you to 
  specify read and write access controls per file or directory level. They can both be good choices, depending on 
  the team's needs.
  
- **Storage Requirements:** Both VCS are similar on the disk space usage, but Git repositories can't handle large 
  binary files, where SVN can store large binary files, as well as code.
  
- **Usability:** Both Git and SVN use the command line as the primary user interface, but SVN commands are easier to 
  use by non-technical users, where git syntax can be overwhelming at first.

- **Performace:** The client-server model of SVN outperforms with large files and codebases.

Here's a simple illustration of how Git and SVN work:

  ![git_vs_svn.png](images/git_vs_svn.png)


## 3. Implementation of the Alternative

Now that we have some knowledge about Subversion, Git and their differences, let's redo this assignment, now with 
Subversion as the Version Control System.

#### Step zero - Environment setup

- For this tutorial, I've downloaded TortoiseSVN 1.14.1 on my machine, you can find it here:
https://tortoisesvn.net/downloads.html

  TortoiseSVN is a really easy to use source control software for Windows. It is based on Apache Subversion and it 
  provides a nice and easy user interface for Subversion.
  

- For the remote repository, I've created an account with Helix TeamHub, that offers free SVN hosting for up to 5 
users and 1 GB of data. It is very user-friendly and has clear set up instructions. Here you can create your 
account: https://www.perforce.com/hth/svn-hosting and here you can browse the set-up instructions: 
https://helixteamhub.cloud/docs/user/Content/HTH-User/subversion.html#Version_control_with_Subversion .
  
  
- The link and credentials to access my repository are:
  - https://helixteamhub.cloud/jaquelineandr/projects/devops_ca1/repositories/devops_ca1/tree/HEAD
  - email:1191844@mailinator.com
  - company ID: jaquelineandr
  - username: user
  - password: 123456789


#### Step one - Create a new folder, set the remote repository and tag the project's initial version

- This time, we can download the project we want to work on, by getting it from the following link:
  https://github.com/spring-guides/tut-react-and-spring-data-rest/tree/master/basic  
  

- Next, we have to create a new folder that will correspond to your local repository. I've named mine "CA1_svn". 
  Inside this folder, you can right-click and select the option "SVN Checkout". Make sure the URL to your 
  repository is correct and click OK. Now we have a working copy (WC).
  

  ![1.png](images/1.png)
  

- Another important step is to create the file structure inside your working directory. You should create a trunk 
  folder, a branches folder and a tags folder.
  

- Finally, you can move all the source code files into the trunk folder.


- Before we commit those new files to the remote repository, we should add some of them to the global ignore pattern 
  (since we don't really need them), like, for example, the .mvn, the .idea, the target and the node modules folders, 
  as well as the package-lock.json file. We can do this by right-clicking on the working directory folder window and 
  selecting the settings option:
  
  
  ![2.png](images/2.png)
  

- Now, we can send all these files to the remote repository. Right click on the file explorer, select "SVN Commit", 
  write your commit message, confirm all the files you want to commit and click OK.
  

  ![3.png](images/3.png)


- Now that all the files are in the repository, we can create a tag to mark this as a stable version of the work. 
  Tags in SVN work very similarly as branches do. A copy of all the files will be kept in a tag folder, marked 
  with the version you specify.
  To create a tag, on the working directory folder, we must right click on the file explorer, select "TortoiseSVN" 
  and then select "Branch/tag". The path to the tag folder you want to create should be specified (/tags/v1.2.0), as 
  well as a log message. In this case, we want to copy all the files of the project, se we'll create a copy in the 
  repository from HEAD revision and then click OK.
  

  ![4.png](images/4.png)


  A complete copy of the project, including the folder structure, is now stored in the folder v1.2.0, in the tags 
  folder of the remote repository. 
  
- Last but not least, we should update our working directory, so that it is syncronized with the remote repository. 
  Now the tags folder is also updated locally.


#### Step two - Creating a branch for the email feature

- As mentioned above, creating a branch and a tag is very similar. In your working copy directory, right-click 
  and select "TortoiseSVN" and then click on the "Branch/tag" option. Make sure to specify the path to the branch's 
  folder and your branch name/message. Also, you should select the option to switch the working copy to the new branch, 
  so you'll be able to work in the branch instead of the trunk:
  

  ![5.png](images/5.png)


#### Step three - Add support for email field, validations and testing

- As was mentioned in the Git tutorial, to successfully run the application, you should run the command:

  `./mvnw spring-boot:run`

- With the project open in your IDE, it's time to make the changes needed: find the Java class Employee, and add a new
  private attribute with the name "emailAddress", of the String data type; add the email property to the equals() 
  and hashcode() methods; add a getter and a setter methods for the email address; add the email property to the 
  toString() method and add the validation methods for all the employee's properties.


- In the DatabaseLoader class, update the creation of the employees with the new email parameter.


- In the app.js file, update the table to include the email field.

  Since the changes made to these files are exactly the same as in the first tutorial, you can check them in the 
  following files: [Employee.java](src/main/java/com/greglturnquist/payroll/Employee.java), [app.js](src/main/js/app.js).


- Now we're ready to compile all the files (click on the maven tool, select Lifecyle and double-click the
  compile option). To check if the changes are rendering, you can go to the link http://localhost:8080/ .
  
  Again, you should have something like this:


  ![browser img.PNG](images/browser img.PNG)

- Lastly, we'll add the same tests to check if the employees are being created smoothly and if the validation of
  their attributes is working correctly. You can consult them here, since the tests are the same as in the previous 
  tutorial: [EmployeeTest.java](src/test/java/EmployeeTest.java) .


#### Step four - Commit all changes and tag the new version

- To commit the files, right-click in your working copy directory and click "SVN Commit". Add your commit message and 
  select all the files you want to include in this commit, then click OK. The commit window will look something like 
  this:
  
  
  ![6.png](images/6.png)


  You can now find the modified files in your remote repository branch.

- Now, to be able to merge, we must first switch to the main working directory (we have been working on the same 
  files, but the remote repository is pointing to the email-field branch). To do so, we'll right-click inside the 
  main working directory, select "TortoiseSVN", select "Switch" and specify the path.
  

- It's now time to merge the email-field branch into trunk: right-click inside the main working directory, select 
  "TortoiseSVN", then "Merge" and select "Merge a range of revisions". Make sure the URL to merge from is the path 
  to the branch folder. You can then select the range by clicking on "Show log" and selecting the latest log. Click 
  "Next" and "Merge". The trunk folder is now updated locally!
  

  ![7.png](images/7.png)
  ![8.png](images/8.png)
  

- Now it's time to commit the previous merge, so that the remote repository will have the latest changes.

  
  ![9.png](images/9.png)


- Lastly, let's create another tag to mark the new version: in your working directory, right-click and select 
  "TortoiseSVN" and then "Branch/tag". The path to the tag folder you want to create should be specified 
  (/tags/v1.3.0), as well as a log message. Again, we want to copy all the files of the project, se we'll 
  create a copy in the repository from HEAD revision and then click OK.
  

  ![10.png](images/10.png)
  

- We can now update our working directory, so that the tags folder is also updated locally.


#### Step five - Fixing bugs (invalid emails)

- Let's create another branch from trunk so we can fix the email issue: right-click in your working copy directory,
  select "TortoiseSVN" and then click on the "Branch/tag" option. Make sure to specify the path to the branch's
  folder and your branch name/message. Don't forget to select the option to switch to the new branch.
  

![11.png](images/11.png)


- To stop employees from being added with invalid email addresses (for example, without the "@" sign or the dot after 
  the email domain), we'll add the regex validation to the email validation method 
  (see [Employee.java](src/main/java/com/greglturnquist/payroll/Employee.java)) and add more tests to verify this 
  validation is done correctly (see [EmployeeTest.java](src/test/java/EmployeeTest.java)).
  

- Now the changes are done and the tests are all passing, we can compile the project (click on the maven tool, 
  select "Lifecycle" and double-click on "compile").
  
- Next, we'll commit the new changes to the branch: right-click in your working copy directory and click "SVN 
  Commit". Add your commit message and select all the files you want to include.


![12.png](images/12.png)


- Before merging, we have to switch to the main working directory again. To do so, we'll right-click inside the
  main working directory, select "TortoiseSVN", select "Switch" and specify the path.


- It's now time to merge the fix-invalid-email branch into trunk: right-click inside the working copy directory, 
  select "TortoiseSVN", then "Merge". Make sure the URL to merge from is the path to the branch folder. You can 
  then select the range by clicking on "Show log" and selecting the latest log. Click "Next" and "Merge".


![13.png](images/13.png)


- To commit this merge, right click on the working copy directory and select "SVN Commit". Don't forget your commit 
  message or to check if all the files to commit are there.


![14.png](images/14.png)


- Let's now create a tag for this new stable version: in your working directory, right-click and select
  "TortoiseSVN" and then "Branch/tag". The path to the tag folder you want to create should be specified
  (/tags/v1.3.1), as well as the log message. Again, we want to copy all the files of the project, se we'll
  create a copy in the repository from HEAD revision and then click OK.


![15.png](images/15.png)

### Final considerations

After trying to do the same assignment using the two different control version tools, I can mention some pros and 
cons to each one, from what I have experienced.

- About Git, it can be a bit difficult to learn, as there are many commands and the right workflow can be a bit complex 
  for non-technical users to learn. It is not difficult _per se_, but it might take more time to learn than SVN. 
  Fortunately, since I was already familiar with Git and its functionalities, this was no longer a problem. 
  One of the features that I preferred in Git, compared to SVN, was the tagging. It seemed much simpler to me to 
  just add a tag to mark the version of the code you want, instead of copying the project into another folder.
  

- SVN used with the TortoiseSVN tool is easy and intuitive to use. The main issue for me would be the branching and 
  tagging functionalities. I found that having too many branches and tags could be confusing, as all the folders of 
  the project are copied. This means that if you create a tag folder to keep a copy of the project, but you have 
  previously created tags and branches, all of those copies will be copied recursively into the new tag folder as 
  well. Also, if you need to create a working copy of the project in your local machine, it might take a very long 
  time, as you'll be downloading several copies of the project.
  
Overall, this was a very beneficial assignment, as I got to get familiar with Git commands and using the command 
line instead of using only the IDE to perform the desired version control tasks. Also, working with another VCS 
was great, as I experimented with a centralized VCS, which was different to what I was used to, and I got to 
understand the pros and cons of both types.



