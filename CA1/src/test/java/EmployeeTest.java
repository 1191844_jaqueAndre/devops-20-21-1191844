import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void createEmployeeWithValidData() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "journey companion";
        String jobRole = "gardener";
        String emailAddress = "samthewise@gmail.com";

        Employee employee = new Employee(firstName, lastName, description, jobRole, emailAddress);

        assertNotNull(employee);
    }

    @Test
    void createEmployeeWithEmptyLastName() {
        String firstName = "Samwise";
        String lastName = "";
        String description = "journey companion";
        String jobRole = "gardener";
        String emailAddress = "samthewise@gmail.com";

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }

    @Test
    void createEmployeeWithEmptyNullFirstName() {
        String firstName = null;
        String lastName = "Gamgee";
        String description = "journey companion";
        String jobRole = "gardener";
        String emailAddress = "samthewise@gmail.com";

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }

    @Test
    void createEmployeeWithDescriptionConsistingOfTwoBlankSpaces() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "  ";
        String jobRole = "gardener";
        String emailAddress = "samthewise@gmail.com";

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }

    @Test
    void createEmployeeWithEmptyJobRole() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "journey companion";
        String jobRole = "";
        String emailAddress = "samthewise@gmail.com";

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }

    @Test
    void createEmployeeWithNullEmail() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "journey companion";
        String jobRole = "gardener";
        String emailAddress = null;

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }

    @Test
    void createEmployeeWithoutAtSymbolInEmail() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "journey companion";
        String jobRole = "gardener";
        String emailAddress = "samthewisegmail.com";

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }

    @Test
    void createEmployeeWithoutDotSymbolInEmail() {
        String firstName = "Samwise";
        String lastName = "Gamgee";
        String description = "journey companion";
        String jobRole = "gardener";
        String emailAddress = "samthewise@gmailcom";

        assertThrows (IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobRole, emailAddress));
    }
}
