# Class Assignment 4 - Containers with Docker

The main goal for this assignment is to use Docker to set up two containers where we will execute an application, 
and then explore some alternative tools or tools that complement Docker, such as Kubernetes or Heroku.

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in 
packages called containers. Containers are isolated from one another and bundle their own software, libraries and 
configuration files; they can communicate with each other through well-defined channels. Because all the containers 
share the services of a single operating system kernel, they use fewer resources than virtual machines.


## 1. Description of the implementation of the Requirements

Before starting with the actual implementation, I've added issues in Bitbucket, to organize the tasks ahead:

   ![Issues.PNG](pics/Issues.PNG)

### 1.1. Installing Docker

Installing Docker is very straightforward. I've downloaded the installer by clicking 
[here](https://www.docker.com/products/docker-desktop). While installing, you will be asked if you want to enable 
Hyper-V Windows features or install required Windows components for WSL 2.

- WSL 2 backend: Windows Subsystem for Linux (WSL) 2 allows Linux containers to run natively without emulation, 
  avoiding having to maintain both Linux and Windows scripts. Docker Desktop uses the dynamic memory allocation 
  feature in WSL 2 to improve the resource consumption.
- Hyper-V backend and Windows containers

I chose the WS 2 backend option. The Docker Desktop installation includes Docker Engine, Docker CLI client, Docker 
Compose, Notary, Kubernetes, and Credential Helper.

Once installed, you can open the Docker application and explore the platform:

![Docker_Window.PNG](pics/Doker_window.PNG)

### 1.2. Creating the project's folders and configuring the containers

I've downloaded the teacher's repository to obtain the folder structure for the project, as well as the docker and 
docker compose file templates, by clicking the following link: 
https://bitbucket.org/atb/docker-compose-spring-tut-demo/.

After creating a folder named "CA4" in my local repository, I've copied the folders db and web, as well as the 
docker compose and .gitignore files into it.

Next, I made some necessary modifications to the docker file for the web container:
- changed the directory to clone to mine:
  
  `RUN git clone https://bitbucket.org/1191844_jaqueAndre/devops-20-21-1191844.git`
  
- changed the workdir:
  
  `WORKDIR devops-20-21-1191844/CA2/Part 2/Demo_Gradle`
  
- corrected the name of the application on the RUN command 
  
  `RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/`
  
- included a command to add permissions to execute gradlew: 
  
  `RUN chmod u+x gradlew`

Here is the **web dockerfile**:

![Web_dockerFile.PNG](pics/Web_dockerFile.PNG)

As we can see, it runs from an image that has tomcat installed. The first instructions allow to update the 
packaging system, to install git, nodejs and npm when starting the container. Next, it will create a folder and 
switch the current working directory to it, so it can then clone my repository inside that folder. After the cloning 
instruction, there's a command to change the current working directory again, but now to the folder where I have the 
application I want to run. Next, there´s a command to add permissions to the gradle wrapper file, so it can be 
executed when building the application, which is the next command. Finally, it copies the generated war file to 
Tomcat's webapps folder. The last command is the configuration of which port I want to use.

The **db dockerfile** remained unchanged:

![Db_dockerFile.PNG](pics/Db_dockerFile.PNG)

This dockerfile will run from an image that has ubuntu installed. The first instructions allow to update the
packaging system, install open jdk 8, unzip and wget when starting the container. Next, it will create a folder in 
the path "usr/src/" called "app/" and switch the current working directory to it. It will then get and install H2 once, 
and run it. Finally, there's a configuration for the ports I want to use.

Here we can see the **docker-compose** file:

![Docker compose.PNG](pics/Docker%20compose.PNG)

The first line of this file describes the file format version. Next, we have the two containers defined int the 
services section. For each service we have defined the folder where the dockerfile is located (web and db). There is a 
configuration for the ports, and also network configuration. On the db service, there's also a configuration for the 
volume folder and a dependency of the web container on the db container. This means that the container db will start 
first.

### 1.3. Starting the containers and running the application

Now that we're all set, it's time to start the containers, by running the command: 

`docker compose up`

We can now open the app in the browser: 

http://localhost:8080/demo-0.0.1-SNAPSHOT/


![Docker_WebApp.PNG](pics/Docker_WebApp.PNG)

And check the H2 console(use the following connection string : jdbc:h2:tcp://192.168.33.11:9092/./jpadb): 
   
http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console/

![Docher_H2.PNG](pics/Docker_H2.PNG)

### 1.4. Publishing the images to Docker Hub

Docker Hub repositories allow you to share container images with your team, customers, or the Docker community at large.
The docker images are pushed to Docker Hub through the `docker push` command. A single Docker Hub repository can 
hold many Docker images (stored as tags).

First, we need to create a repository on Docker Hub. To do so, I've created an acount (by opening on this link : 
https://hub.docker.com and following the instructions). Then I clicked on "Repositories" and on "Create Repository". 
You just need to insert a name for the repository (I've chosen "devops20-21"), a description (not mandatory) and 
click "create".

Since I logged in by the Docker Hub website when I created the account, I didn't need to take any additional steps, 
but if you already have a Docker Hub account, a repository and haven't logged in yet, you could do so by running 
the following command:

`docker login`


To check the images that are available in my machine, I ran the command:

![Docker_images.PNG](pics/Docker_images.PNG)



So we can send both images, we'll use the commands: 
    
`docker push ca4_web:latest`

`docker push ca4_db:latest`

"ca4_web" and "ca4_db" are the names of my containers, "latest" is the tag that attributed to them by default. 
Unfortunately, this didn't work:

![Request_Denied.PNG](pics/Request_denied.PNG)

I thought it might have been a login issue, so I logged out and logged back in:

 `docker logout`
    
![Login.PNG](pics/Login.PNG)
    
After trying the same push commands, I got the same error message.

I read up on possible causes for this error and found the solution on a Docker documentation page: re-tagging an 
existing local image docker tag (`<existing-image><hub-user>/<repo-name>[:<tag>]`).

So I ran the commands:

`docker tag 6a397ba0a571 jaquelineandre/devops20-21:ca4_db`

`docker tag fb525cb65a10 jaquelineandre/devops20-21:ca4_web`

Success! Now we have the following images:

![New_images.PNG](pics/New_images.PNG)

I pushed the db image first:

`docker push jaquelineandre/devops20-21:ca4_db`

![Pushed_image.PNG](pics/Pushed_image.PNG)

And then I pushed the web one:

![Pushed_imageweb.PNG](pics/Pushed_imageweb.PNG)

You can now see on the Docker Hub repository that the images are there:

![Docker_Hub.PNG](pics/Docker_Hub.PNG)


### 1.5. Copy the database file to a volume

Volumes are the preferred mechanism for persisting data generated by and used by Docker containers. 

First, we need to start a shell on the db container. For that, I ran the following command:

`docker exec -it ca4_db_1 /bin/bash`

...where ca4_db_1 is the name of the database container.

Since the volume was already configured in the docker-compose file, I didn't need to do that. The folder ./data is 
binded to the folder /usr/src/data in the container, which means that any file we copy into the /usr/src/data folder 
inside the container will also be copied to the ./data in my host machine, and vice-versa.

The h2 jar file from the app folder in the db container was copied to the data folder using the cp command:

`cp /usr/src/app/h2-1.4.200.jar /usr/src/data`

The file now appears in the data folder on my host machine: 

![Database_file.PNG](pics/Database_file.PNG)


To stop the containers, you can run the commands:

`docker-compose stop db`
`docker-compose stop web`

## 2. Analysis of the alternative : Kubernetes

Kubernetes (also known as k8s or "kube") is an open source container orchestration platform that automates many of
the manual processes involved in deploying, managing, and scaling containerized applications. It works with a range
of container tools and runs containers in a cluster, often with images built using Docker. It was originally
developed by Google, but it is now maintained by the Cloud Native Computing Foundation.

As mentioned before, Docker is a standalone software that can be installed on any computer to run containerized 
applications. It is what enables us to run, create and manage containers on a single operating system. In this sense,
Kubernetes could also be used to manage these containers, created by docker images. This is not very 
practical, though, as Kubernetes is mainly used to deploy and manage complex containerized applications.

Now, if you have Docker installed on a bunch of hosts (different operating systems), then you can leverage Kubernetes. 
It can then allow you to automate container provisioning, networking, load-balancing, security and scaling across 
all these nodes from a single command line or dashboard.

So, in this case, we can't exactly say that Kubernetes is an alternative to Docker. Kubernetes can run without Docker 
and Docker can function without Kubernetes, but Kubernetes can (and does) benefit greatly from Docker and vice versa.
Docker even has its own orchestration engine, called Docker Swarm, but Kubernetes has become so popular that Docker 
Desktop comes with its own Kubernetes distribution.

### 2.1 Kubernetes architecture

- **Node**: common term for virtual machines and/or bare-metal servers that Kubernetes manages. There are two types 
  of nodes. One is the Master Node, where the heart of Kubernetes is installed. It controls the scheduling of pods 
  across various worker nodes (a.k.a just nodes), where your application actually runs. The master node’s job is to 
  make sure that the desired state of the cluster is maintained. We can have multiple master nodes so that 
  Kubernetes can survive even the failure of a master node.
  

- **Pod**: basic unit of deployment in Kubernetes. It is a collection of related Docker containers that need to 
  coexist. For example, your web server may need to be deployed with a redis caching server so you can encapsulate 
  the two of them into a single pod. Kubernetes deploys both of them side by side.
  

- **Service**: set of containers that work together providing, for example, functioning of a multi-tier 
  application. Kubernetes supports the dynamic naming and load balancing of Pods by using abstractions. This 
  approach ensures a transparent connection between services by the name, and allows you to track their current state.


- **Labels**: key/value pairs that are bound to Pods and other objects or services, in addition to allowing to group 
  them easily and assign tasks.
  

- **Namespaces**: method that allows for logically dividing the united Kubernetes cluster to multiple virtual 
  clusters. Each virtual cluster can exist within a virtual environment limited by quotas without having an impact 
  on other virtual clusters.
  
![Kubernetes.png](pics/Kubernetes.png)

1. _**kubelet**_: This relays the information about the health of the node back to the master as well as execute 
instructions given to it by master node.
2. _**kube-proxy**_: This network proxy allows various microservices of your application to communicate with each 
   other, 
   within the cluster, as well as expose your application to the rest of the world, if you so desire. Each pod can 
   talk to every other pod via this proxy, in principle.
   
### 2.2. Networking

Docker provides three network modes for network communication between containers:

- **Bridge**: Docker automatically creates a layer-3 network bridge and configures masquerading rules for the external 
  network interface, using the network address translation (NAT) principle, which allows containers to communicate 
  with each other and connect to external networks.
  
- **Host**: In this mode a host network driver ensures that a container is not isolated from the Docker host. The 
  container shares the host network stack, and the container’s host name is the same as the host name of the host 
  operating system.

- **None**: No IP addresses are configured for a given container’s network interface except the 127.0.0.1 address 
  for the loopback interface. There is no access to external networks if the None network mode is set.


Very briefly, with Kubernetes, pods are the basic units of the container cluster. Each pod has its own IP address and 
consists of at least one container. A pod can consist of multiple containers which are used for related tasks. 
Containers of the same Pod cannot open the same port simultaneously. This type of restriction is used because a Pod 
that consists of multiple containers still has a single IP address. This is called the "IP-per-pod" model.

![kubernetes-Networking-Model.png](pics/kubernetes-Networking-Model.png)


## 3. Implementation of the alternative

### 3.1. Activating Kubernetes on Docker Desktop

Docker Desktop has a Kubernetes option integrated in it, that allows us to start a Kubernetes single-node cluster when 
starting Docker Desktop. This means that a standalone instance of Kubernetes will be installed, running as a Docker 
container.

Kubernetes itself runs in containers. When you deploy a Kubernetes cluster you first install Docker and then use
tools like kubeadm which starts all the Kubernetes components in containers. Docker Desktop does all that for you.

It's quite easy to activate it: I've opened the Docker application, clicked on settings and then, on Kubernetes. 
I've chosen the "Enable Kubernetes" option.

![Enable_Kubernetes.PNG](pics/Enable_Kubernetes.PNG)

You can now see the Kubernetes logo as well, on the bottom left of your Docker Desktop application screen.

When working with Docker, we managed the containers with the docker and docker-compose command lines. 
Kubernetes uses a different tool called **kubectl** to manage apps - Docker Desktop installs this for you.

We can now check the state of our Docker Desktop cluster by running the command:

`kubectl get nodes`

![Kube_nodes.PNG](pics/Kube_nodes.PNG)

This single-node we see in the image above is a full Kubernetes cluster, with a single node that runs the 
Kubernetes API.

Now, various parts of Kubernetes are running, and you can see them as containers if you run the command:

`docker info`

![Kube_containers.PNG](pics/Kube_containers.PNG)

### 3.2. Configuring the Kubernetes manifests

To create the necessary yaml files, I could go one of two ways: create them from zero or use a tool to convert the 
docker-compose file into the Kubernetes necessary files. I chose the second option, since it's still complex to 
write yaml files from scratch specifically for Kubernetes (especially when you have no experience).

The tool I've used is called Kompose. Kompose is a conversion tool for Docker Compose to container orchestrators 
such as Kubernetes (or OpenShift). I have installed it via Chocolatey, using the command line as administrator:

`choco install kubernetes-kompose`

Then I created a new folder called CA4_Kubernetes, where I pasted the docker-compose file I used for the first part of 
this assignment. In the command line (inside this folder), I ran the command:

`kompose convert`

Inside my folder I now have 5 different yaml files: db-deployment.yml, db-service.yml, default-networkpolicy.yml, 
web-deployment.yml and web-service.yml. I removed the now unnecessary docker-compose file.

Although they were almost ready to use, some changes had to be made to these files:
- On the deployment files, the correct image had to be specified. I used the ones I published on my Docker Hub 
  repository on the first part of the assignment. So for the db container I used the 
  "jaquelineandre/devops20-21:ca4_db" and for the web container I used the "jaquelineandre/devops20-21:ca4_web".
- On the service files, I added a "type: LoadBalancer" on the spec definition.
- On the networkpolicy file, I changed the apiVersion to networking.k8s.io/v1.

Now that the files were ready, I used the command:
`kubectl apply -f .`

![Kubectl_apply.PNG](pics/Kubectl_apply.PNG)

We can see that the configuration files were created successfully. The containers are now running:

![running_containers.PNG](pics/running_containers.PNG)

Before we try to access the h2 console or the app in the browser, we need to make another small change: the 
application.properties file in the app project has the wrong database connection IP address specified. To change 
this, I needed to open a bash shell inside the web container:

`kubectl exec -it web-8657864b55-xn2qt sh`

Then, I changed the current working directory to the folder where the application.properties file was located 
(src/main/resources). Since there was no editor installed, I installed nano:

`apt-get -y install nano`

I opened the file by typing:

`nano application.properties`

Remember when I said I modified the service spec so it would be of type LoadBalancer? This allowed me to obtain an 
external IP, so I could access the app through the host computer. Now, I want to replace the IP address in the string

`spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE` 

to

`spring.datasource.url=jdbc:h2:tcp://db.default.svc.cluster.local:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE`

"db.default.svc.cluster.local" is an internal domain defined by Kubernetes to each service. This domain is known, as 
it is a Kubernetes standard. By using this, I won't need to change the IP on this file again, even if the machine 
stops and restarts.

Next, we need to build these changes, by running the command:

`./gradlew clean build`

And we also need to copy the resulting war file into the tomcat app folder:

`cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/`

All done! We can finally access the app through the browser and the H2 console (now using the connection 
string: jdbc:h2:tcp://db.default.svc.cluster.local:9092/./jpadb). We can do so by accessing the links:

http://localhost:8080/demo-0.0.1-SNAPSHOT/

http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console/

![Kubernetes_web.PNG](pics/Kubernetes_web.PNG)

![Kubernetes_h2.PNG](pics/Kubernetes_h2.PNG)


A few commands I found useful to explore the pods and nodes are:

`kubectl get pods`

![Get_pods.PNG](pics/Get_pods.PNG)

`kubectl get pods -o wide`

![get_pods_wide.PNG](pics/get_pods_wide.PNG)

`kubectl get nodes`

![Get_nodes.PNG](pics/Get_nodes.PNG)

`kubectl describe pods <pod name>` - This brings a good deal of information about the pod configuration

`kubectl get services -o wide`

![get_services.PNG](pics/get_services.PNG)


### 3.3. Copy the database file to a volume

Unfortunately I could not find enough information on how to persist a file located inside a container to a folder in 
the host machine. Theoretically, adding this block of code into the db-service.yaml file would suffice:

```        
volumeMounts:
  - mountPath: /usr/src/data
    name: volume-data
  volumes:
    - name: volume
      hostPath:
        path: ./data
        type: Directory
```

Unfortunately, I could not make this work and the db file was not copied to the data folder in the host machine.

## 4. Final thoughts

This assignment was very useful to understand how and when to use docker, as well as explore Kubernetes as a 
complementary tool. I enjoyed working with both, but I must say that Kubernetes is very complex and I've only 
skimmed the surface on its capabilities and functionalities. As I mentioned above, Kubernetes was not exactly meant 
to only be used as an alternative to Docker, as I chose to do here in the assignment, but more of an enhancer that 
allows 
to manage containers across clusters of hosts.

I will be sure to study more about both Docker and Kubernetes and practice with their functionalities after this 
assignment, as they are tools that will be useful for years to come.


